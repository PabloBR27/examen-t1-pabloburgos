﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Models
{
    public class Post
    {
        public int ID { get; set; }
        public string Autor { get; set; }
        public string Titulo { get; set; }
        public string Contenido { get; set; }
        public string FechaPub { get; set; }
    }
}
