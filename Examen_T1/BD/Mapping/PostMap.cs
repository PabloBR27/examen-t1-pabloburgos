﻿using Microsoft.EntityFrameworkCore;
using Examen_T1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Examen_T1.BD.Mapping
{
    public class PostMap: IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Post");
            builder.HasKey(Post => Post.ID);
        }
    }
}
