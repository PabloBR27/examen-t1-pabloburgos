﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen_T1.BD;
using Examen_T1.Models;
using Microsoft.AspNetCore.Mvc;


namespace Examen_T1.Controllers
{
    public class PostDetalle
    {
        public Post Poost { get; set; }
        public List<Comentario> Comentarios { get; set; }
    }
    public class PostController : Controller
    {
        private PostContext context;
        public PostController(PostContext context)
        {
            this.context = context;
        }

        public IActionResult MostrarPost()
        {
            List<Post> ListaPosts = context.Posts.ToList();

            return View("MostrarPost", ListaPosts);
        }
        public ViewResult DetallePost(int id)
         {
             var poost = context.Posts;

             Post post = poost.FirstOrDefault(item => item.ID== id);

             return View(post);
        }
    }
}
